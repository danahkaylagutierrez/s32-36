
//use directive keyword require to make a module in the current file/module
const express = require('express');

//express function is our server stored in a constant variable app
const app = express();


//require mongoose module to be used in our entry point file
const mongoose = require('mongoose');
const cors = require('cors')
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.r2a4x.mongodb.net/courseBooking?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true});

const port = 3001;



//express.json is an express framework to parse incoming json payloads
app.use(express.json());
app.use(express.urlencoded({extended :true}));
app.use(cors());

//connecting userRoutes to index.js entry point
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');


//Notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', ()=>{
	console.log('Connected to Database')
})


/*
//routes
	//htpps://localhost:3001/users

		
	//Request
		//params
		//body
		..url
		//HTTP Methods
			//GET
			//POST
			//PuT
			//DELETE
			//UPDATE
		//headers
			//authorization


app.get('/users', (req,res)=>{
	res.send('hello')
})

app.post('/users', (req,res)=>{
	//console.log(req.body) //contains objects
	//console.log(req)
	let name = req.body.name
	res.send(`Hello ${name}`)
})
*/


//middleware entry point url( rot url before any endpoints)
app.use('/api/users',userRoutes)
app.use('/api/courses',courseRoutes)


app.listen(port,()=>{
	console.log(`Server is running at ${port}`)
})