const Course = require('./../models/Course');

const auth = require('./../auth');

const bcrypt = require('bcrypt');


//TO CREATE A COURSE
module.exports.createCourse = (reqBody)=>{

	const {courseName, description, price} = reqBody

	const newCourse = new Course({
		courseName: courseName,
		description : description,
		price : price
	})

	return newCourse.save().then((result,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})
}

//TO GET ALL COURSES
module.exports.getAllCourses= () => {
	
	return Course.find().then( (result, error) => {
		if(result !== null){
			return result
		} else {
			return error
		}
	})
}

//TO GET ALL ACTIVE COURSE
module.exports.getActiveCourses=()=>{
	return Course.find({isActive:true}).then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})

}


//GET A SCPECIFIC COURSE
module.exports.getSpecificCourse = (reqBody)=>{
	//console.log(reqBody)

	return Course.findOne({courseName:reqBody}).then( (result, error) => {
		if (result == null){
			return 'Course not found'
		}else{
			//if matching document found, return document
			if(result){
				return result
			}else{
				return error
			}
		}
	})
}



//GET BY ID
module.exports.getCourseById = (courseId) => {
	// console.log(userId)
	// console.log(reqBody)

	return Course.findById(courseId).then( (result,error) => {
		//console.log(result)
		if(error){
			return false
		} else {
			return result
		}
	})

}