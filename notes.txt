1//initialize node package manager (NPM)
			npm init


2//express.js is a 3rd party library (framework)
	//install express.js
		npm install express

3//mongoose is a package needs to be installed to be used to interact with the server
	//npm install mongoose

4. Connect your project 
	//get connection string from mongodb atlas
	//use connect() method
		//update password of the conection string
		//change the name of the dtabase of the connection string

5. Optional: Add notif oof whether database is connected

6. Create a Schema

7. Create a Model out of the Schema
	//why? to make use of Model Methods
		//.findbyId()
		//findOne()

8. Create a route
	Note: add parsers to convert jsnon payloads to express framework
	app.use(express.json());
	app.use(express.urlencoded({extended :true}));

9:) Create .gitignore file
//so that when you push your project in the online repository, all unnecessary files for the project will be ignored 
		touch .gitignore



		define: 
			get - retrieve from database
			post - add/create
			put- update
			delete -delete


10:) Install nodemon

11.:) Packages to be added:
		- CORS (connecting browser to server)
			npm i cors
		- Bcrypt (hashing password)
			npm i bcrypt
		- JSON Web Token
			npm i jsonwebtoken 