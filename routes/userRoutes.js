

const express = require('express');

//Create routes
//.Router() function is a function that handles requests


//Router() handles the requests
const router = express.Router();

const userController = require('./../controllers/userController');
const auth = require('./../auth');

//syntax: router.HTTPmethod('url',<request listener>)
/*
router.get('/', (req, res)=>{
		console.log('Hello from userRoutes')
})
*/

//Routes will only handle requests and responses
//http://localhost:3001/users

router.post('/email-exists',(req,res)=>{
	//invoke the function here
	userController.checkEmail(req.body.email).
	then(result => res.send(result))
});

//register route
router.post('/register', (req,res)=>{
	//expecting data/object coming from the request body
	userController.register(req.body).then(result => res.send(result))
})

//route to get all user
router.get('/', (req,res)=>{
	userController.getAllUsers().then(result =>res.send(result))
})



router.post('/login', (req,res)=>{
	userController.login(req.body).then(result =>res.send(result))
								 //ininvoke na parameter from controllers
})





router.get('/details', auth.verify, (req, res)=>{
	// console.log(req.headers.authorization)
	// userController.getUserProfile(req.body).then(result =>res.send(result))
	let userData = auth.decode(req.headers.authorization);

	console.log(auth.decode(req.headers.authorization))
	userController.getUserProfile(req.body).then(result =>res.send(result))
})



//edit user information
router.put('/:userId/edit', (req,res) =>{
	// console.log(req.params)
	//console.log(req.body)
	const userId = req.params.userId

	userController.editProfile(userId,req.body).then(result =>res.send(result))
})

//edit user information using token
router.put('/edit', auth.verify, (req,res) =>{
	
	let userId = auth.decode(req.headers.authorization).id;
	console.log(userId)

	userController.editProfile(userId,req.body).then(result =>res.send(result))
})






//mini activity
router.put('/edit-profile',(req,res) =>{
	//console.log(req.body) (go to postman , add new request "update user using email as filter, then sa body, just copy the json sa ibang user, change some details lang like name, then makikita mo sa console log yun after sending the request")
userController.editDetails(reqBody).then(result =>res.send(result))
})


router.delete('/:userId/delete', (req,res)=>{
	 // console.log(req.params) 
	 // [make another request sa postman na delete method, then try to delete somtehing]
userController.delete(req.params.id).then(result =>res.send(result))

})

router.delete('/:userId/delete', (req,res)=>{
	// console.log(auth.decode(req.headers.authorization).email)

	const email =auth.decode(req.headers.authorization).email

	userController.deleteUser(email).then(result =>res.send(result))
})


router.post('/enroll', auth.verify,(req,res)=>{
let data ={
	//user id of the logged in user
	userId : auth.decode(req.headers.authorization).id,
	//course id of the course you are enrolling in, to be supllied by the user
	courseId: req.body.courseId
}

userController.enroll(data).then(result => res.send(result))

 })


 router.post('/new-enroll',auth.verify, (req,res)=>{

 	let data ={
		userId : auth.decode(req.headers.authorization).id,
	   courseId:req.body.courseId

	}
 })

module.exports = router;



